ALTER FUNCTION F_VALIDA_CARACTERES(
	  @CHAR10 AS CHAR(10))
	RETURNS BIT
AS

/*  
    --------------------------------------------------------------------------------------------------
    -- VERSÃO - DATA ALTERACAO - USUARIO - CHAMADO - DESCRIÇÃO
    --------------------------------------------------------------------------------------------------
    -- V001 - 25/04/2022 - CHRISTIANL -		- CRIAÇÃO DA FUNC.
    --------------------------------------------------------------------------------------------------
    -- FORMA DE UTILIZAR
    --------------------------------------------------------------------------------------------------
	[dbo].[F_VALIDA_CARACTERES]
	('USUARIO')
    --------------------------------------------------------------------------------------------------
    -- TESTAR
    --------------------------------------------------------------------------------------------------
    PRINT [dbo].[F_VALIDA_CARACTERES]
	('4001MARIA')
*/

BEGIN

	DECLARE @CONFERE AS BIT

	IF (REPLACE(REPLACE(@CHAR10, ' ', ''), CHAR(13)+CHAR(10), '') <> @CHAR10)
		SELECT @CONFERE = 0

	ELSE IF (@CHAR10 IS NULL)
		SELECT @CONFERE = 0

    ELSE IF (@CHAR10 = '')
		SELECT @CONFERE = 0

    ELSE
        SELECT @CONFERE = 1

	RETURN @CONFERE
END
