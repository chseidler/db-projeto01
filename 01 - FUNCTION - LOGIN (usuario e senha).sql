ALTER FUNCTION F_LOGIN_VALIDA_USUARIO_SENHA(
	  @USUARIO AS CHAR(10)
	, @SENHA AS CHAR(10))
	RETURNS BIT
AS

/*  
    --------------------------------------------------------------------------------------------------
    -- VERSÃO - DATA ALTERACAO - USUARIO - CHAMADO - DESCRIÇÃO
    --------------------------------------------------------------------------------------------------
    -- V001 - 25/04/2022 - CHRISTIANL -		- CRIAÇÃO DA FUNC.
    --------------------------------------------------------------------------------------------------
    -- FORMA DE UTILIZAR
    --------------------------------------------------------------------------------------------------
	[dbo].[F_LOGIN_VALIDA_USUARIO_SENHA]
	('USUARIO', 'SENHA')
    --------------------------------------------------------------------------------------------------
    -- TESTAR
    --------------------------------------------------------------------------------------------------
    PRINT [dbo].[F_LOGIN_VALIDA_USUARIO_SENHA]
	('4001MARIA','SENHAM')
*/

BEGIN

	DECLARE @CONFERE AS BIT

	IF EXISTS (SELECT 1 FROM TREINA_USUARIOS
				WHERE USUARIO = @USUARIO AND SENHA = @SENHA)
		SELECT @CONFERE = 1

	ELSE
		SELECT @CONFERE = 0

	RETURN @CONFERE
END
