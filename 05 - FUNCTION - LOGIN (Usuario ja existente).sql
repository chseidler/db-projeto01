ALTER FUNCTION F_VERIFICA_USUARIO_EXISTENTE(
	@USUARIO AS CHAR(10))
	RETURNS BIT
AS

/*  
    --------------------------------------------------------------------------------------------------
    -- VERSÃO - DATA ALTERACAO - USUARIO - CHAMADO - DESCRIÇÃO
    --------------------------------------------------------------------------------------------------
    -- V001 - 25/04/2022 - CHRISTIANL -		- CRIAÇÃO DA FUNC.
    --------------------------------------------------------------------------------------------------
    -- FORMA DE UTILIZAR
    --------------------------------------------------------------------------------------------------
	[dbo].[F_VERIFICA_USUARIO_EXISTENTE]
	('USUARIO')
    --------------------------------------------------------------------------------------------------
    -- TESTAR
    --------------------------------------------------------------------------------------------------
    PRINT [dbo].[F_VERIFICA_USUARIO_EXISTENTE]
	('4001MARIA')
*/

BEGIN

	DECLARE @RETORNO AS BIT

	SELECT @RETORNO =
		CASE
			WHEN EXISTS( SELECT 1 FROM TREINA_USUARIOS WHERE USUARIO = @USUARIO )
				THEN 1
				ELSE 0
		END

	RETURN @RETORNO
END
