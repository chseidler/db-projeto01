ALTER PROCEDURE P_CADASTRAR_USUARIO(
			@USUARIO AS CHAR(10)
		,	@SENHA AS CHAR(10)
		,	@NOME AS VARCHAR(20)
		,	@RETORNO AS CHAR(1) OUTPUT)
AS

/*  
    --------------------------------------------------------------------------------------------------
    -- VERSÃO - DATA ALTERACAO - USUARIO - CHAMADO - DESCRIÇÃO
    --------------------------------------------------------------------------------------------------
    -- V001 - 26/04/2022 - CHRISTIANL -		- CRIAÇÃO DA PROC.
    --------------------------------------------------------------------------------------------------
    -- RETORNOS
    --------------------------------------------------------------------------------------------------
    '0' - Não foi possível cadastrar. Usuário já existente.
    '1' - Usuário cadastrado com sucesso.
    '2' - Nome de usuário não aceito.
    '3' - Senha não aceita.
    '4' - Ocorreu um erro inesperado ao cadastrar o usuário.
    --------------------------------------------------------------------------------------------------
    -- FORMA DE UTILIZAR
    --------------------------------------------------------------------------------------------------
    DECLARE @RESULTADO AS CHAR(1)

	EXEC P_CADASTRAR_USUARIO
        'CHSEIDLER'
    ,   '123'
    ,   'CHRISTIAN SEIDLER'
	,	@RETORNO = @RESULTADO OUTPUT
    --------------------------------------------------------------------------------------------------
    -- TESTAR
    --------------------------------------------------------------------------------------------------
    DECLARE @RESULTADO AS CHAR(1)

	EXEC P_CADASTRAR_USUARIO
        'CHSEIDLER'
    ,   '123'
    ,   'CHRISTIAN SEIDLER'
	,	@RETORNO = @RESULTADO OUTPUT

	PRINT @RESULTADO
*/

SET NOCOUNT ON

--
	DECLARE @DIAS_VALIDOS AS TINYINT = 45
--

BEGIN
BEGIN TRY

    IF (dbo.F_VALIDA_CARACTERES(@USUARIO) = 0)
        SET @RETORNO = '2'

    ELSE IF (dbo.F_VALIDA_CARACTERES(@SENHA) = 0)
        SET @RETORNO = '3'

	ELSE IF (dbo.F_VERIFICA_USUARIO_EXISTENTE(@USUARIO) = 1)
		SET @RETORNO = '0'
	
    ELSE
        BEGIN

            DECLARE @USUARIO_ATUALIZACAO AS CHAR(10) = SYSTEM_USER
            DECLARE @DATA_ATUALIZACAO AS DATETIME = GETDATE()
	        DECLARE @DATA_VALIDADE_SENHA AS DATE = DATEADD(DAY, @DIAS_VALIDOS, @DATA_ATUALIZACAO)

            INSERT INTO TREINA_USUARIOS (USUARIO, SENHA, NOME, USUARIO_ATUALIZACAO, DATA_ATUALIZACAO, DATA_VALIDADE_SENHA)
			VALUES (@USUARIO, @SENHA, @NOME, @USUARIO_ATUALIZACAO, @DATA_ATUALIZACAO, @DATA_VALIDADE_SENHA)

            SET @RETORNO = '1'
        END

END TRY

BEGIN CATCH

    SET @RETORNO = '4' -- + ERROR_MESSAGE() (enviar para um log?)

END CATCH
END
