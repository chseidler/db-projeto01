ALTER PROCEDURE P_VALIDADE_SENHA_MODIFICA_DIAS
	@DIAS_VALIDOS AS TINYINT	
AS

/*  
    --------------------------------------------------------------------------------------------------
    -- VERSÃO - DATA ALTERACAO - USUARIO - CHAMADO - DESCRIÇÃO
    --------------------------------------------------------------------------------------------------
    -- V001 - 25/04/2022 - CHRISTIANL -		- CRIAÇÃO DA PROC.
    --------------------------------------------------------------------------------------------------
    -- FORMA DE UTILIZAR
    --------------------------------------------------------------------------------------------------
	EXEC P_VALIDADE_SENHA_MODIFICA_DIAS
        @DIAS_VALIDOS = 45
    --------------------------------------------------------------------------------------------------
    -- TESTAR
    --------------------------------------------------------------------------------------------------
    SELECT TOP (1000) DATEDIFF(DAY, DATA_ATUALIZACAO, DATA_VALIDADE_SENHA)
	FROM [DB_Projeto_01].[dbo].[TREINA_USUARIOS]
*/

SET NOCOUNT ON

BEGIN
	UPDATE TREINA_USUARIOS
	SET DATA_VALIDADE_SENHA = DATEADD(DAY, @DIAS_VALIDOS, DATA_ATUALIZACAO)
END
