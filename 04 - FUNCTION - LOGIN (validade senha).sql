ALTER FUNCTION F_LOGIN_VALIDA_VALIDADE_SENHA(
	@USUARIO AS CHAR(10))
	RETURNS BIT
AS

/*  
    --------------------------------------------------------------------------------------------------
    -- VERSÃO - DATA ALTERACAO - USUARIO - CHAMADO - DESCRIÇÃO
    --------------------------------------------------------------------------------------------------
    -- V001 - 25/04/2022 - CHRISTIANL -		- CRIAÇÃO DA FUNC.
    --------------------------------------------------------------------------------------------------
    -- FORMA DE UTILIZAR
    --------------------------------------------------------------------------------------------------
	[dbo].[F_LOGIN_VALIDA_VALIDADE_SENHA]
	('USUARIO')
    --------------------------------------------------------------------------------------------------
    -- TESTAR
    --------------------------------------------------------------------------------------------------
    PRINT [dbo].[F_LOGIN_VALIDA_VALIDADE_SENHA]
	('4001MARIA')
*/

BEGIN

	DECLARE @CONFERE AS BIT

	IF EXISTS (SELECT 1 FROM TREINA_USUARIOS
				WHERE USUARIO = @USUARIO AND DATA_VALIDADE_SENHA >= GETDATE())
		SELECT @CONFERE = 1

	ELSE
		SELECT @CONFERE = 0

	RETURN @CONFERE
END
