ALTER PROCEDURE P_VALIDA_LOGIN( -- V002
			@USUARIO AS CHAR(10)
		,	@SENHA AS CHAR(10)
		,	@RETORNO AS CHAR(1) OUTPUT) -- V002
AS

/*  
    --------------------------------------------------------------------------------------------------
    -- VERSÃO - DATA ALTERACAO - USUARIO - CHAMADO - DESCRIÇÃO
    --------------------------------------------------------------------------------------------------
    -- V001 - 25/04/2022 - CHRISTIANL -		- CRIAÇÃO DA PROC.
    -- V002 - 26/04/2022 - CHRISTIANL -     - CORREÇÃO NOME DA PROCEDURE, TAMANHO VARCHAR RETORNO
    --------------------------------------------------------------------------------------------------
    -- RETORNOS
    --------------------------------------------------------------------------------------------------
    '0' - Usuário e/ou senha inválido(s).
    '1' - Login realizado com sucesso.
    '2' - Senha expirada, favor atualizar.
    --------------------------------------------------------------------------------------------------
    -- FORMA DE UTILIZAR
    --------------------------------------------------------------------------------------------------
    DECLARE @RESULTADO AS CHAR(1)

	EXEC P_VALIDA_LOGIN
        'CHSEIDLER'
    ,   '123'
	,	@RETORNO = @RESULTADO OUTPUT
    --------------------------------------------------------------------------------------------------
    -- TESTAR
    --------------------------------------------------------------------------------------------------
    DECLARE @RESULTADO AS CHAR(1)

	EXEC P_VALIDA_LOGIN
        'CHSEIDLER'
    ,   '123'
	,	@RETORNO = @RESULTADO OUTPUT

	PRINT @RESULTADO
*/

SET NOCOUNT ON

BEGIN
	IF (dbo.F_LOGIN_VALIDA_USUARIO_SENHA(@USUARIO, @SENHA) = 0)
        SET @RETORNO = '0' -- V002
	
    ELSE IF (dbo.F_LOGIN_VALIDA_VALIDADE_SENHA(@USUARIO) = 1)
		SET @RETORNO = '1' -- V002

    ELSE
        SET @RETORNO = '2' -- V002
END
